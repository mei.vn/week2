<?php

class stringCalculator
{
    const MAX_NUMBER = 1000;
    public function add($string)
    {
        $sum = 0;
       if($string == ''){
           return 0;
       }
       else{
           $numbers = $this->convert_number($string);
        //    return array_sum(array_map(function($number){
        //     if($number > self::MAX_NUMBER){
        //         return 0;
        //     }
        //     if($number > 0){
        //       return $number;
        //     }
        //     else throw new InvalidArgumentException('Invalid number : '.$number);
        //    } ,$numbers));
           foreach($numbers as $number){
              if($number < 0){
               throw new InvalidArgumentException('Invalid number: '.$number);
               }
               if(!is_numeric($number))
               $number = 0;
               if($number > self::MAX_NUMBER){
                   continue;
               }
                $sum += $number;
           }
       }
       return $sum;
    }
    public function convert_number($string){

        $stripped = preg_split("/\s*(n|[^0-9_-]|[a-z]|[A-Z])\s*/", $string);
        return $stripped;
    }
}
