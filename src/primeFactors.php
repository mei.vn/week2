<?php

class primeFactors
{
    

    public function generate($number)
    {
        $res = [];
        $tmp = 2;
        while($number > 1){
            while($number % $tmp == 0){
                $res[] = $tmp;
                $number /= $tmp;
            }
            $tmp ++ ;
        }
        return  $res;
    }


}
