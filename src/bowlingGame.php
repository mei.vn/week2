<?php

class bowlingGame
{
    protected $score; protected $arr_scores = [];

    public function roll($pins)
    {
        if($pins < 0 || $pins > 10){
            throw new invalidArgumentException;
        }
        $this->arr_scores[] = $pins;
      
      
    }
     public function score()
    {
        $score = 0;
        $shot = 0;
        
        for($frames = 1; $frames <=  10 ; $frames ++){

            if($this->isStrike($this->arr_scores, $shot)){
                 $score += 10 + $this->arr_scores[$shot + 1] + $this->arr_scores[$shot + 2];
                 $shot += 1;

            }
            else if($this->isSpares($this->arr_scores, $shot)){
   
                     $score += 10 +  $this->arr_scores[$shot + 2];
                       $shot += 2;

 
            }
            else{
              
                $score += $this->arr_scores[$shot] + $this->arr_scores[$shot + 1];
                $shot += 2;

            }
            // $shot += 2;  
           
        }
                // check spare
        return $score;
        // TODO: write logic here
    }
    public function isStrike($arr, $shot){
        if( $arr[$shot] == 10){
            return true;
        }
        return false;
    }
    public function isSpares($arr, $shot){
        if( $arr[$shot] + $arr[$shot + 1] == 10){
            return true;
        }
        return false;
    }
}

