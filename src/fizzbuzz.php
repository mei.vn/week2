<?php

class fizzbuzz
{
    public function change($number)
    {
        $res = [];
        if($number <= 0){
            return 0;
        }
        for($i = 1; $i <= $number; $i ++){
                if($i % 15 == 0)
                {
                    $res[] = 'fizzbuzz';
                }
               else if($i % 3 == 0)
                {
                    $res[] = 'fizz';
                }
                else if($i % 5 == 0){
                    $res[] =  'buzz';
                }
                else {
                    $res[] = $i;
                }
        }
        return $res;
    }
}
