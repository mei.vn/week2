<?php
namespace tennis;
class Player{
    protected $name;
    protected $score;
    public function __construct($t_name, $t_score){
        $this->name = $t_name;
        $this->score = $t_score;
    }
    public function getName(){
        return $this->name;
    }
    public function getScore(){
        return $this->score;
    }
    public function setScore($score){
         $this->score = $score;
    }
 

}