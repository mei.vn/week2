<?php
namespace tennis;
class tennisScoring
{
    protected $arr_res = [0 =>'Love', 1=>'Fifteen', 2=>'Thirty', 3=>'Forty'];
    protected $player1;
    protected $player2;
    public function __construct(Player $player1,Player $player2){
        $this->player1 = $player1;
        $this->player2 = $player2;
    }
    public function score()
    {

        if($this->player1->getScore() == 0 && $this->player2->getScore() == 0){
            return 'Love';
        }
        if($this->player1->getScore() == $this->player2->getScore()){
            // Deuce
            if($this->hasEnoughPoint()){
                return  'Deuce';
            }
            else{
                // check tied
                 return  $this->arr_res[$this->player1->getScore()] .'-'.'All';
            }
        } 
        else{
            // winner
            if($this->hasWinner()){
                return 'Winner is '.$this->the_winner()->getName();
            }
            //advatage
            else if($this->hasAdvatage()){
                return 'Advantage '.$this->the_winner()->getName();
            }
            else{
                return $this->arr_res[$this->player1->getScore()] .'-'.$this->arr_res[$this->player2->getScore()];
            }
            }
            //normal

    }  
    private function the_winner(){
    if($this->player1->getScore() > $this->player2->getScore()){
        return $this->player1;
    }
    else return $this->player2;
}
private function hasEnoughPoint(){
    return $this->player1->getScore() - $this->player2->getScore() >=4 ||  $this->player2->getScore() - $this->player1->getScore() >=4;
}
private function isLeadingByTwo(){
    return $this->player1->getScore() - $this->player2->getScore() >=2 || $this->player2->getScore() - $this->player1->getScore() >=2;
}
private function isLeadingByOne(){
    return $this->player1->getScore() - $this->player2->getScore() >=1 || $this->player2->getScore() - $this->player1->getScore() >=1;
}
private function hasAdvatage(){
    return $this->hasEnoughPoint() && $this->isLeadingByOne();
}
public function hasWinner(){
       return  $this->hasEnoughPoint() && $this->isLeadingByTwo();
}
}
