<?php

namespace spec;

use numberToRoman;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class numberToRomanSpec extends ObjectBehavior
{
     function it_convert_0(){
      $this->convert(0)->shouldReturn('the number must be large than 0');
  }
  function it_convert_1_to_I(){
      $this->convert(1)->shouldReturn('I');
  }
  function it_convert_2_to_II(){
    $this->convert(2)->shouldReturn('II');
}
function it_convert_5_to_V(){
    $this->convert(5)->shouldReturn('V');
}
function it_convert_6_to_VI(){
    $this->convert(6)->shouldReturn('VI');
}
function it_convert_10_to_X(){
    $this->convert(10)->shouldReturn('X');
}
function it_convert_11_to_XI(){
    $this->convert(11)->shouldReturn('XI');
}
function it_convert_20_to_XX(){
    $this->convert(20)->shouldReturn('XX');
}
function it_convert_50_to_L(){
    $this->convert(50)->shouldReturn('L');
}
function it_convert_58_to_LVIII(){
    $this->convert(58)->shouldReturn('LVIII');
}
function it_convert_89(){
    $this->convert(89)->shouldReturn('LXXXIX');
}
function it_convert_1024_to_MXXIV(){
    $this->convert(1024)->shouldReturn('MXXIV');
}
function it_convert_400_to_MXXIV(){
    $this->convert(400)->shouldReturn('CD');
}
    function it_convert_600(){
    $this->convert(600)->shouldReturn('DC');
}
function it_convert_700_to_DCC(){
    $this->convert(700)->shouldReturn('DCC');
}
function it_convert_800_to_DCCC(){
    $this->convert(800)->shouldReturn('DCCC');
}
function it_convert_99(){
    $this->convert(99)->shouldReturn('XCIX');
}
function it_convert_4999(){
    $this->convert(4999)->shouldReturn('MMMMCMXCIX');
}
function it_convert_5000(){
    $this->convert(5000)->shouldReturn('MMMMM');
}
function it_convert_4534(){
    $this->convert(4534)->shouldReturn('MMMMDXXXIV');
}

}
