<?php

namespace spec;

use fizzbuzz;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class fizzbuzzSpec extends ObjectBehavior
{
    function it_change_0()
    {
        $this->change(0)->shouldBe(0);
    }
    function it_change_1()
    {
        $this->change(1)->shouldBe([1]);
    }
    function it_change_10()
    {
        $this->change(10)->shouldBe([1,2,'fizz',4,'buzz','fizz',7,8,'fizz','buzz']);
    }
    function it_change_15()
    {
        $this->change(15)->shouldBe([1,2,'fizz',4,'buzz','fizz',7,8,'fizz','buzz',11,'fizz',13,14,'fizzbuzz']);
    }
}
