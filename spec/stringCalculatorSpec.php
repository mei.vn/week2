<?php

namespace spec;

use stringCalulator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class stringCalculatorSpec extends ObjectBehavior
{
    function it_empty_string()
    {
        $this->add('')->shouldEqual(0);
    }
    function it_2_2_string()
    {
        $this->add('2,2')->shouldEqual(4);
    }
    function it_2_liner_2_string()
    {
        $this->add('2\n2')->shouldEqual(4);
    }
    function it_invalid_string(){
         $this->shouldThrow(new \InvalidArgumentException('Invalid number: -10' ))->duringAdd('-10');
    }
    function it_invalid_space(){
          $this->add('2,    2')->shouldEqual(4);
    }
     function it_invalid_special_letter(){
          $this->add('3*3')->shouldEqual(6);
    }
       function it_invalid_special_letter_2(){
          $this->add('3&$3')->shouldEqual(6);
    }
           function it_invalid_special_letter_3(){
          $this->add('3a3')->shouldEqual(6);
    }
     function it_invalid_special_letter_4(){
          $this->add('3affaaa3')->shouldEqual(6);
    }
         function it_invalid_space_and_special_letter(){
          $this->add('3affa    aa3')->shouldEqual(6);
    }
}
