<?php

namespace spec;

use bowlingGame;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class bowlingGameSpec extends ObjectBehavior
{
    public function it_roll_0_21()
    {
        for($shot = 0 ; $shot < 20; $shot++){
            $this->roll(0);
        }
        $this->score()->shouldreturn(0);
    }
    public function it_roll_1_21()
    {
        for($shot = 0 ; $shot < 20; $shot++){
            $this->roll(1);
        }
        $this->score()->shouldreturn(20);
    }
    public function it_spares(){
        $this->roll(5);
        $this->roll(5);
        $this->roll(7);
        for($shot = 0 ; $shot < 17; $shot++){
            $this->roll(0);
        }
        $this->score()->shouldbe(24);
    }
    public function it_spares_1(){
        $this->roll(5);
        $this->roll(5);
        $this->roll(7);
        for($shot = 0 ; $shot < 17; $shot++){
            $this->roll(1);
        }
        $this->score()->shouldreturn(41);
    }
    public function it_strike_0(){
        $this->roll(10);
        $this->roll(4);
        $this->roll(5);
        for($shot = 0 ; $shot < 17; $shot++){
            $this->roll(0);
        }
        $this->score()->shouldbe(28);
    }
    public function it_strike_1(){
        $this->roll(10);
        $this->roll(4);
        $this->roll(5);
        $this->rollTimes(16,1);
        $this->score()->shouldbe(44);
    }
    public function it_strike_1_2(){
        $this->roll(10);
        $this->roll(4);
        $this->roll(5);
        $this->roll(2);
        $this->rollTimes(15,1);
        $this->score()->shouldbe(45);
    }

    public function it_invalid_pin(){
        $this->shouldThrow('InvalidArgumentException')->duringRoll(-10);
    }
       public function it_invalid_pin_11(){
        $this->shouldThrow('InvalidArgumentException')->duringRoll(11);
    }
    public function it_perfect_run(){
        $this->rollTimes(12,10);
        $this->score()->shouldreturn(300);
    }
    public function it_last_frame_Spares(){
    $this->rollTimes(18,1);
    $this->roll(5);
    $this->roll(5);
    $this->roll(7);
    $this->score()->shouldreturn(35);
}
    public function it_last_frame_strikes(){
    $this->rollTimes(18,1);
    $this->roll(10);
    $this->roll(5);
    $this->roll(6);
    $this->score()->shouldreturn(39);
}
    private function rollTimes($count, $pin){
        for($shot = 0 ; $shot < $count ; $shot++){
            $this->roll($pin);
        }

    }
  
}
