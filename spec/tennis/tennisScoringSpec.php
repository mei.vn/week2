<?php
namespace spec\tennis;
use tennisScoring;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use tennis\Player;
class tennisScoringSpec extends ObjectBehavior
{
    protected $thuong;
    protected $john;

    function it_0_0()
    {
        $this->score()->shouldBe('Love');
    }
    function let(){
        $this->thuong = new Player('Thuong', 0);
        $this->john = new Player('John', 0);
        $this->beConstructedWith( $this->thuong, $this->john);
    }
    function it_0_1()
    {
        $this->john->setScore(1);
        $this->score()->shouldBe('Love-Fifteen');
    }
    function it_2_0()
    {
        $this->thuong->setScore(2);
        $this->score()->shouldBe('Thirty-Love');
    }
    function it_4_3()
    {
        $this->thuong->setScore(4);
        $this->john->setScore(3);
        $this->score()->shouldBe('Advantage Thuong');
    }
    function it_4_4()
    {
        $this->thuong->setScore(4);
        $this->john->setScore(4);
        $this->score()->shouldBe('Deuce');
    }
    function it_2_2()
    {
        $this->thuong->setScore(2);
        $this->john->setScore(2);
        $this->score()->shouldBe('Thirty-All');
    }
    function it_5_3()
    {
        $this->thuong->setScore(5);
        $this->john->setScore(3);
        $this->score()->shouldBe('Winner is Thuong');
    }
    function it_3_5()
    {
        $this->thuong->setScore(3);
        $this->john->setScore(5);
        $this->score()->shouldBe('Winner is John');
    }
    function it_4_0()
    {
        $this->thuong->setScore(4);
        $this->score(4, 0)->shouldBe('Winner is Thuong');
    }
    function it_8_9()
    {
        $this->thuong->setScore(8);
        $this->john->setScore(9);
        $this->score(4, 0)->shouldBe('Advantage John');
    }
  
}

